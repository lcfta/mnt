#!/usr/bin/env bash
DATE=$(date +%Y.%m.%d-%H%M%S.%N)
GITROOT=$(git rev-parse --show-toplevel)

[ ! -e ~/.config/rclone/rclone.conf ] && echo "please configure rclone before mounting" && exit 1

REMOTES=$(grep "\[.*\]" ~/.config/rclone/rclone.conf | sed 's#\[\(.*\)\]#\1#')

for remote in $REMOTES 
do 
  fusermount -u $GITROOT/$remote
done
