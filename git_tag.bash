#!/usr/bin/env bash
GIT_PS1_SHOWDIRTYSTATE='y'
GIT_PS1_SHOWSTASHSTATE='y'
GIT_PS1_SHOWUNTRACKEDFILES='y'
GIT_PS1_DESCRIBE_STYLE='contains'
GIT_PS1_SHOWUPSTREAM='auto'

source ./git-prompt.sh
__git_root ()
{
    printf -- '%s' "$(git rev-parse --show-toplevel 2> /dev/null)"
}

__git_head ()
{
    [ ! -z "$(__git_root)" ] && printf -- '[%s -=%s=-]' "$(__git_root)" "$(git rev-parse --short HEAD 2> /dev/null)" || printf "[not tracked in git]"
}

echo `__git_head``__git_ps1`
