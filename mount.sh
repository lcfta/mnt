#!/usr/bin/env bash
DATE=$(date +%Y.%m.%d-%H,%M,%S.%N)
GITROOT=$(git rev-parse --show-toplevel)
GITTAG=$(./git_tag.bash)
MNTLOGS=$GITROOT/mnt.logs
LOGHEAD="== MOUNT $@ $DATE $GITTAG=="
echo `pwd`

mkdir -p $MNTLOGS
[ ! -e ~/.config/rclone/rclone.conf ] && echo "please configure rclone before mounting" && exit 1

nohup_rclone_mount () {
  rclonename=$1
  echo rclonename $rclonename
  mountpoint=$2
  echo mountpoint $mountpoint
  logfilename="${rclonename%?}-_-$DATE"
  echo "OUT $LOGHEAD" | tee $MNTLOGS/${logfilename}_stdout.log
  echo "ERR $LOGHEAD" > $MNTLOGS/${logfilename}_stderr.log
  nohup rclone -vv mount $rclonename $mountpoint --allow-other --vfs-cache-mode writes --vfs-read-chunk-size 512M >>$MNTLOGS/${logfilename}_stdout.log 2>>$MNTLOGS/${logfilename}_stderr.log &
}

REMOTES=$(grep "\[.*\]" ~/.config/rclone/rclone.conf | sed 's#\[\(.*\)\]#\1#')

for remote in $REMOTES 
do 
  mkdir -p $remote 
  sleep 0.5
  nohup_rclone_mount $remote: $GITROOT/$remote
done
