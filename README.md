## Mount the Cloud with Rclone

Usage: `./mount.sh`

`mount.sh` will find all of your remotes from the `rclone.conf` file, make mount points for them and then `nohup` the rclone mount command to attach them while logging the results into a locally created `mnt.log` directory into timestamped logs.
